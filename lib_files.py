import os
from lib_additions import add_getTimeBounds, add_timeStamp, add_date


def fil_get_files(fromfolders, file_dict={}):
    global leftDate, rightDate
    leftDate, rightDate = add_getTimeBounds()


    while fromfolders:
        for item in os.listdir(fromfolders[0]):
            file_path = '%s\\%s' % (fromfolders[0], item)
            if os.path.isdir(file_path):
                fromfolders.append(file_path)
            else:
                if fil_filter(file_path):
                    file_dict.update({file_path: os.path.getsize(file_path)})

        fromfolders.remove(fromfolders[0])
    return file_dict

def fil_filter(file):
    if file[-3::] == 'bak':
        year    = int(add_timeStamp(os.path.getctime(file)).strftime('%Y'))
        month   = int(add_timeStamp(os.path.getctime(file)).strftime('%m'))
        day     = int(add_timeStamp(os.path.getctime(file)).strftime('%d'))

        if leftDate <= add_date(year, month, day) < rightDate:  #leftDate <=
            return True

    return False