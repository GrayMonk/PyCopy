import os, sys
from datetime import datetime, date, timedelta

### Decorating
def dec_cls():
    os.system('cls')

def dec_StartInfo():
    return [' ' * 26 + 'PyCopy. Версия от 02.03.2018', '=' * 80, '\n']

def dec_printSeparator():
    print()

def add_now():
    return datetime.now()

def add_curdir():
    pth = sys.argv[0].split('\\')
    pth.pop()
    return os.path.abspath('\\'.join(pth))


def add_getTimeBounds():
    # Функция возврщает левую и правую границу дат,
    # т.е. вчерашний и сегодняшний день в 00 часов 00 минут.

    last = datetime.now() - timedelta(days=1)
    return date(last.year, last.month,
                last.day), date(datetime.now().year,
                                datetime.now().month,
                                datetime.now().day)  ## вчера и сегодня

def add_timeStamp(value):
    return datetime.fromtimestamp(value)

def add_date(y, m, d):
    return date(y, m, d)