import smtplib
from lib_additions import add_curdir, add_now as now


def log_startTime():
    log_text = '{}: Запуск программы.'.format(now())
    writeLog(log_text)

def log_closeTime():
    log_text = '{}: Завершение программы.'.format(now())
    writeLog(log_text)

def log_error(text):
    log_text = '{}: {}'.format(now(), text)
    writeLog(log_text)
    send_mail(log_text)

def writeLog(log_text):
    log_file = open(add_curdir() + '\\log.txt', 'a')
    log_file.write(log_text + '\n')
    log_file.close()

def send_mail(msg):
    smtpObj = smtplib.SMTP('192.168.1.202', 25)
    smtpObj.starttls()
    smtpObj.login('smanahov@orion26.com', '**********')
    toList = ['it@orion26.com']
    smtpObj.sendmail('PyCopy', 'smanahov@orion26.com', msg.encode('utf-8'))
    smtpObj.quit()
