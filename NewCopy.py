from lib_additions import *
from lib_init import *
from lib_log import *
from lib_files import fil_get_files
from lib_copy import startcopy


log_startTime()


#Получаем пути
fromCopy, intoCopy = init_start()
if not fromCopy:
    msg = 'Ошибка. Отсутствуют начальный(-е) пути копирования.'
    print(msg)
    log_error(msg)
    os._exit(0)

if not intoCopy:
    msg = 'Ошибка. Отсутствуют результатирующий(-е) пути копирования.'
    print(msg)
    log_error(msg)
    os._exit(0)

if not checkFreeSpace(intoCopy[0]):
    msg = 'Внимание!!! На диске NAS-Volume1 заканчивается свободное место. Дальнейшее копирование невозможно.'
    print(msg)
    log_error(msg)
    os._exit(0)

#Получаем сисок файлов
files = fil_get_files([x for x in fromCopy])
if not files:
    msg = 'Ошибка. Не найдены файлы для копирования.'
    print(msg)
    log_error(msg)
    os._exit(0)

#Копируем
startcopy(files, fromCopy, intoCopy)


log_closeTime()