import os, subprocess, ctypes
from lib_log import log_error, log_closeTime
from lib_additions import add_curdir

def init_start():
    # Читает файл copymap.txt, содержищий пути копирования
    # <f> (откуда) и <t> (куда).

    copymapPath = add_curdir() + '\\copymap.txt'
    if os.path.exists(copymapPath):
        fromCopy, intoCopy = [], []
        for line in open(copymapPath):
            if line.startswith('f'):
                fromCopy.append((line[2::]).strip())
            elif line.startswith('t'):
                intoCopy.append((line[2::]).strip())

        fromCopy, intoCopy = checkPathes(fromCopy, intoCopy)
        return fromCopy, intoCopy
    else:
        errors('INIT_ERR_0', add_curdir())


def checkPathes(data1, data2, FalseList=[]):
    # Проверяет существование папок, откуда необходимо производить копирование.
    # Возвращает только существующие папки.

    # проверяем путь ИЗ
    for path in data1:
        if not os.path.exists(path):
            FalseList.append(path)
            msg = 'Точка копирования "{}" не найдена или она недоступна. Исключаю ее из плана копирования.'.format(
                path)
            print(msg)
            log_error(msg)

    for value in FalseList:
        if value in data1:
            data1.remove(value)

    # проверяем путь В
    FalseList = []
    netPathLog = '\\\\NAS\\Volume_1 /USER:Agent *********'

    for path in data2:
        if not os.path.exists(path):
            try:
                command = 'cmd /c "net use {}: {}"'.format(path[:1], netPathLog)
                subprocess.call(command)
            except:
                msg = 'Ошибка подключения диска.'
                print(msg)
                log_error(msg)
            finally:
                if not os.path.exists(path):
                    FalseList.append(path)
                    msg = 'Не удается подключить сетевой диск по пути "{}". Возможно диск недоступен или путь не существует.'.format(path)
                    print(msg)
                    log_error(msg)

    for value in FalseList:
        if value in data2:
            data2.remove(value)

    return data1, data2


def errors(code, *args):
    error_text = ''
    if code == 'INIT_ERR_0':
        error_text = '{}. Не найден файл <copymap.txt> по следующему пути: {}'.format(code, args[0])

        print(error_text)
        log_error(error_text)

    print('Завершение программы.')
    log_closeTime()
    os._exit(0)


def checkFreeSpace(path):

    free_bytes = ctypes.c_ulonglong(0)
    ctypes.windll.kernel32.GetDiskFreeSpaceExW(ctypes.c_wchar_p(path[0:3]), None, None, ctypes.pointer(free_bytes))

    if free_bytes.value / 1024 / 1024 < 100000:
        return False
    else:
        return True
