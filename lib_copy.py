import threading, sys, time, os, shutil
from lib_additions import dec_StartInfo
from lib_log import *


def startcopy(files, fromcopy, intocopy):
    # Отправная точка. Имеем словарь типа "файл: размер", пути FROM и INTO
    # Здесь же печать на экран.
    #
    # Задачи этой функции:
    #   сформировать глобальные переменные
    #   выщимлять по одной паре "файл - размер" из словаря и запукать:
    #       поток 1: копирование
    #       поток 2: определение размера копируемого файла для визуализации
    #       поток 3: прогрессбар. 
    #
    # По итогу возвращяем True или False

    global body                 # для печати
    global isCopying, cap       # флаг процесса копирования, объем файла

    body = []
    l  = 0                              # номер копируемого файла
    isCopying = False                   # сейчас копируется?

    for path in intocopy:
        for obj, capacity in files.items():
            # ------------------------------ На печать -------------------- #
            l += 1
            body.append(' ' * 14 + '%s/%s: %s' % (l, len(files), obj.split('\\')[-1]))

            if len(body) > 10:
                body.remove(body[0])
            # ------------------------------ На печать -------------------- #

            # ----------------- Проверка и создание папок ----------------- #
            obj_copying = check_folder(obj, fromcopy, path)
            if not obj_copying:
                msg = 'Не доступна целевая папка копирования, или не удалось создать папку. Объект: <%s>.' % obj
                print(msg)
                log_error(msg)
                os._exit(0)
            # ----------------- Проверка и создание папок ----------------- #

            cap = 1
            a = ProgressBar(capacity, obj_copying)

            thread1 = threading.Thread(target=copyfile, args=(obj, obj_copying,))

            thread1.start()
            a.start()

            thread1.join() #собираем поток

def check_folder(obj, fromcopy, intocopy):
    for x in fromcopy:
        if x in obj:
            path = obj.replace('%s\\' % x, '').split('\\')[:-1:]
            for y in path:
                if not os.path.exists('%s\\%s' % (intocopy, y)):
                    os.makedirs('%s\\%s' % (intocopy, y))
                intocopy += '\\%s' % y
            return (intocopy + '\\' + obj.split('\\')[-1::][0])
    return False


def update_cap(file):
    global cap
    if isCopying:
        if os.path.getsize(file):
            cap = os.path.getsize(file)
        else:
            cap = 0

def copyfile(obj, obj_copying):
    global isCopying
    isCopying = True

    try:
        shutil.copy2(obj, obj_copying)
    except:
        msg = 'Возникла ошибка во время копирования файла: <%s>.' % obj_copying.split('\\')[-1]
        print(msg)
        log_error(msg)
        os._exit(0)

    time.sleep(2)
    isCopying = False

class ProgressBar():

    def __init__(self, capmax, path):
        self.done   = 0
        self.undone = 100
        self.proc   = 0
        self.capmax = capmax
        self.path   = path

    def printer(proc, progress):

        print(' ' * 26 + 'PyCopy. Версия от 21.03.2018')
        print('=' * 80 + '\n')
        for i in body:
            print(i)

        print('\n' * (11 - len(body)))
        print('[ {} ] {}%'.format(progress, int(proc * 1.43)))

    def start(self, progress = ''):
        while isCopying:
            time.sleep(1)
            update_cap(self.path)
            self.proc = round(70 / (self.capmax - 1) * cap)
            self.undone = 70 - self.proc
            self.done = 70 - self.undone

            os.system('cls')

            progress = '#' * self.done + '.' * self.undone
            ProgressBar.printer(self.proc, progress)
